#+
# Setup script for VADS
# Usage:
# source setup_VADS.sh <DIVA_INSTALL_DIR>
# 
# DIVA_INSTALL_DIR: Directory where DIVA is installed
#

DIVA_INSTALL_DIR=$1
if [ -z "$DIVA_INSTALL_DIR" ]
then
  echo "Usage: source ./setup_VADS.sh <DIVA_INSTALL_DIR>"
else
  source ${DIVA_INSTALL_DIR}/setup_DIVA.sh
  SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
  echo ${SCRIPT_DIR}
  export PYTHONPATH=${SCRIPT_DIR}/model_experiments/:${SCRIPT_DIR}:$PYTHONPATH
  export SPROKIT_PYTHON_MODULES=processes:$SPROKIT_PYTHON_MODULES
fi
